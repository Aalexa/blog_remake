function searchButtonClick() {
   let searchPop = document.getElementById("searchPopUp");
   searchPop.classList.add("show");
    let body = document.querySelector("body");
    body.classList.add("noScroll");
 }

function searchPopUpClose() {
    let searchPop = document.getElementById("searchPopUp");
    searchPop.classList.remove("show");
    let body = document.querySelector("body");
    body.classList.remove("noScroll");
}